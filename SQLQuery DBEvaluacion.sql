
CREATE SCHEMA Sesion
CREATE SCHEMA Sistema

ALTER SCHEMA Sesion Transfer dbo.TblUsuarios
ALTER SCHEMA Sesion Transfer dbo.TblTokens
ALTER SCHEMA Sesion Transfer dbo.TblModulos
ALTER SCHEMA Sesion Transfer dbo.TblMenus
ALTER SCHEMA Sesion Transfer dbo.TblUsuariosPorInstitucion
ALTER SCHEMA Sesion Transfer dbo.TblUsuariosPorRoles
ALTER SCHEMA Sesion Transfer dbo.TblRoles
ALTER SCHEMA Sesion Transfer dbo.TblRolesPorMenus
ALTER SCHEMA Sistema Transfer dbo.TblInstituciones

