var UrlApi = "http://localhost:64315/api/"
function AgregarUsuario() {
    var settings = {
        "url": UrlApi + "AgregarUsuario",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "TxtNombres": $("#TxtNombres").val(),
            "TxtApellidos": $("#TxtApellidos").val(),
            "TxtDireccion": $("#TxtDireccion").val(),
            "TxtEmail": $("#TxtEmail").val(),
            "TxtPassword": $("#TxtPassword").val()
        }),
    };

    $.ajax(settings).done(function (response) {
        //console.log(response);
        alert("Usuario creado correctamente");
        LimpiarFormulario();
        ObtenerUsuarios();
    });
}

function LimpiarFormulario() {
        $("#TxtNombres").val(""),
        $("#TxtApellidos").val(""),
        $("#TxtDireccion").val(""),
        $("#TxtEmail").val(""),
        $("#TxtPassword").val("")
}

function ObtenerUsuarios() {
$("#tblUsuarios td").remove();
    
    var settings = {
        "url": UrlApi + "ObtenerUsuarios",
        "method": "GET",
        "timeout": 0,
      };
      
      $.ajax(settings).done(function (response) {
        //console.log(response);
        LimpiarFormulario();

        $.each(response, function(index, data) {
            var fila = "<tr><td>" + data.TxtNombres +
            "</td><td>" + data.TxtDireccion +
            "</td><td>" + data.TxtEmail +
            "</td><td class='text-center'><a href='#' onclick='ObtenerDatosUsuario("+ data.IdUsuario +");'>Editar</a>" +
            "</td><td class='text-center'><a href='#' onclick='EliminarUsuario("+ data.IdUsuario +");'>Eliminar</a></td></tr>";
            
            $(fila).appendTo("#tblUsuarios")
        });
      });

}

function EliminarUsuario(IdUsuario) {
    var settings = {
        "url": UrlApi + "EliminarUsuario",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "IdUsuario": IdUsuario
        }),
    };

    $.ajax(settings).done(function (response) {
        $.each(response, function(index, data) {
            if(data.Resultado > 0){
                alert("Usuario eliminado correctamente");
                LimpiarFormulario();
                ObtenerUsuarios();
            }else{
                alert("Error, el usuario no fue  eliminado");
            }
        });
    });
}

function ObtenerDatosUsuario(IdUsuario) {
    var settings = {
        "url": UrlApi + "ObtenerDatosUsuario",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "IdUsuario": IdUsuario
        }),
    };

    $.ajax(settings).done(function (response) {

        LimpiarFormulario();
        $("#IdOculto").val(IdUsuario);

        $.each(response, function(index, data) {
            $("#TxtNombres").val(data.TxtNombres);
            $("#TxtApellidos").val(data.TxtApellidos);
            $("#TxtDireccion").val(data.TxtDireccion);
            $("#TxtEmail").val(data.TxtEmail);
            $("#TxtPassword").val(data.TxtPassword);
        });
    });
}

function ActualizarUsuario() {
    var settings = {
        "url": UrlApi + "ActualizarUsuario",
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "IdUsuario": $("#IdOculto").val(),
            "TxtNombres": $("#TxtNombres").val(),
            "TxtApellidos": $("#TxtApellidos").val(),
            "TxtDireccion": $("#TxtDireccion").val(),
            "TxtEmail": $("#TxtEmail").val(),
            "TxtPassword": $("#TxtPassword").val()
        }),
    };

    $.ajax(settings).done(function (response) {
        $.each(response, function(index, data) {
            if(data.Resultado > 0){
                alert("Usuario actualizado correctamente");
                LimpiarFormulario();
                ObtenerUsuarios();
            }else{
                alert("Error, el usuario no fue  actualizado");
            }
        });
        
    });
}

 function Guardar() {
    if($("#IdOculto").val() > 0){
        ActualizarUsuario();
    }else{
        AgregarUsuario();
    }
}

