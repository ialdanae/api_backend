/*
	AUTOR:		ilmer aldana
	FECHA:		08/08/2020
*/
--PROCEDIMIENTO PARA AGREGAR UN USUARIO
ALTER PROC Sesion.SPAgregarUsuario	(
											@_TxtNombres		NVARCHAR(50)
											,@_TxtApellidos		NVARCHAR(50)
											,@_TxtDireccion		NVARCHAR(150)
											,@_TxtEmail			NVARCHAR(100)
											,@_TxtPassword		NVARCHAR(130)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdUsuario),0)
	FROM	Sesion.TblUsuarios	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO Sesion.TblUsuarios	(
											IdUsuario
											,TxtNombres
											,TxtApellidos
											,TxtDireccion
											,TxtEmail
											,TxtPassword	
											,IdUsuarioIngresadoPor
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtNombres
											,@_TxtApellidos
											,@_TxtDireccion
											,@_TxtEmail
											,@_TxtPassword
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 


/*
	AUTOR: Illmer aldana 
	FECHA: 15/08/2020
*/

CREATE PROC Sesion.SPObtenerUsuarios
AS
BEGIN
	SELECT	a.IdUsuario,
			CONCAT(a.TxtNombres,' ',a.TxtApellidos) AS TxtNombres,
			a.TxtDireccion,
			a.TxtEmail,
			a.TxtPassword,
			a.FechaDeIngreso,
			a.IntEstado
	FROM Sesion.TblUsuarios AS a
	WHERE a.IntEstado > 0
END --FIN

/*

	AUTOR: ilmer aldana 
	FECHA: 15/08/2020
*/

CREATE PROC Sesion.SPObtenerDatosUsuario(
	@_IdUsuario INT
)

AS
BEGIN
	SELECT	a.IdUsuario ,
			a.TxtNombres,
			a.TxtApellidos,
			a.TxtDireccion,
			a.TxtEmail,
			a.TxtPassword
	FROM	Sesion.TblUsuarios AS a
	WHERE	a.IdUsuario = @_IdUsuario
END

/*

	AUTOR: Ilmer Aldana
	FECHA:
	15/08/2020
*/

CREATE PROC Sesion.SPEliminarUsuario (
	@_IdUsuario INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	Sesion.TblUsuarios
			SET		IntEstado = 0
					
			WHERE	IdUsuario = @_IdUsuario

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdUsuario
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END



/*

	AUTOR: Ilmer Aldana
	FECHA: 15/08/2020
*/

CREATE PROC Sesion.SPActualizarUsuario (
	@_IdUsuario INT,
	@_TxtNombres NVARCHAR(50),
	@_TxtApellidos NVARCHAR(50),
	@_TxtDireccion NVARCHAR(150),
	@_TxtEmail NVARCHAR(100),
	@_TxtPassword NVARCHAR(130)
)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	Sesion.TblUsuarios
			SET		TxtNombres = @_TxtNombres,
					TxtApellidos = @_TxtApellidos,
					TxtDireccion = @_TxtDireccion,
					TxtEmail = @_TxtEmail,
					TxtPassword = @_TxtPassword
					
			WHERE	IdUsuario = @_IdUsuario

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdUsuario
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END


/*

	AUTOR: Ilmer Aldana
	FECHA: 29/08/2020

*/

CREATE PROC Sesion.SPInicioDeSesion(

	@_TxtEmail			NVARCHAR(100),
	@_TxtPassword		NVARCHAR(130),
	@_TxtToken			NVARCHAR(250),
	@_VigenciaEnMinutos	INT

)
AS
DECLARE
	
	@_IdUsuario		INT = 0,
	@_TxtUsuario	NVARCHAR(100) = '',
	@_UltimoId		INT = 0,
	@_IntResultado	TINYINT = 0,
	@_FilasAfectadas	TINYINT = 0,
	@_IdInstitucion SMALLINT = 0

BEGIN
	SELECT	@_IdUsuario = a.IdUsuario,
			@_TxtUsuario = CONCAT(a.TxtNombres,' ',a.TxtApellidos),
			@_IdInstitucion = b.IdUsuario
	FROM		Sesion.TblUsuarios AS a  
	LEFT JOIN	Sesion.TblUsuariosPorInstitucion AS b
	ON			b.IdUsuario = a.IdUsuario
	WHERE	a.TxtEmail = @_TxtEmail
			AND	a.TxtPassword = @_TxtPassword
			AND a.IntEstado = 1

	BEGIN TRAN

		SELECT @_UltimoId = ISNULL(MAX(a.IdToken),0)
		FROM Sesion.TblTokens AS a

		UPDATE	Sesion.TblTokens
		SET		IntEstado = 0
		WHERE	IdUsuario = @_IdUsuario
		AND		IntEstado > 0

		BEGIN TRY
			INSERT INTO Sesion.TblTokens (IdToken, IdUsuario, TxtToken, VigenciaEnMinutos)
			VALUES (@_UltimoId + 1, @_IdUsuario, @_TxtToken, @_VigenciaEnMinutos)
			SET @_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET @_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_IntResultado = 1
				COMMIT
			END
		ELSE
			BEGIN
				SET @_IntResultado = 0
				SET @_TxtToken     = 'Usuario o contrase�a inv�lida'
				ROLLBACK
			END

		--Devolver Resultado
		SELECT
			IntResultado = @_IntResultado,
			TxtToken = @_TxtToken,
			TxtUsuario = @_TxtUsuario,
			IdInstitucion = @_IdInstitucion
END	


/*
	AUTOR: Ilmer Aldana 
	FECHA: 29/08/2020
*/

--FUNCION PARA VERIFICAR SI EL TOKEN NO HA EXPIRADO

CREATE FUNCTION Sesion.FnVerificarVigenciaToken(
	@_TxtToken	NVARCHAR(250)
)

RETURNS TINYINT
AS
BEGIN
	DECLARE @_IntResultado			TINYINT = 0,
			@_VigenciaEnMinutos		INT = 30,
			@_FechaYHoraDeCreacion	DATETIME = '2001-01-01 01:01:01.001',
			@_FechaYHoraActual		DATETIME = getdate(),
			@_TiempoDeUsoEnMinutos	INT = 0

	SELECT	@_VigenciaEnMinutos = a.VigenciaEnMinutos,
			@_FechaYHoraDeCreacion = a.FechaIngreso

	FROM Sesion.TblTokens AS a
	WHERE	a.TxtToken = @_TxtToken
	AND		a.IntEstado = 1

	SET		@_TiempoDeUsoEnMinutos = DATEDIFF(MINUTE, @_FechaYHoraDeCreacion, @_FechaYHoraActual)

	IF(@_TiempoDeUsoEnMinutos > @_VigenciaEnMinutos)
		BEGIN
			SET @_IntResultado = 0
		END
	ELSE
		BEGIN
			SET @_IntResultado = 1
		END

	RETURN @_IntResultado
END



/*
	AUTOR: Ilmer Aldana 
	FECHA: 29/08/2020
*/

--ACTUALIZAR LA VIGENCIA DEL TOKEN EN CADA TRANSACCION
CREATE PROC Sesion.SPActualizarVigenciaToken (
	@_TxtToken NVARCHAR(250)
)
AS
BEGIN
	--ELIMINAR TOKENS EXPIRADOS
	DELETE Sesion.TblTokens
	WHERE IntEstado = 0

	UPDATE	Sesion.TblTokens
	SET		FechaIngreso = getdate()
	WHERE	TxtToken = @_TxtToken
	AND		IntEstado = 1
END

/*
	AUTOR: Ilmer Aldana
	FECHA: 29/08/2020
*/

--OBTENER ESTADO DEL TOKEN
CREATE PROC Sesion.SPObtenerEstadoToken(
	@_TxtToken NVARCHAR(250)
)
AS
DECLARE @_EstadoToken TINYINT = 0
BEGIN
	--0 = Expirado, 1 = Vigente
	SELECT @_EstadoToken = Sesion.FnVerificarVigenciaToken(@_TxtToken)

	IF(@_EstadoToken = 1)
		BEGIN
			EXEC Sesion.SPActualizarVigenciaToken @_TxtToken
		END

	SELECT EstadoToken = @_EstadoToken

END



/*
	AUTOR: Ilmer Aldana 
	FECHA: 12/09/2020

*/
-- SP PARA GENERAR MENU CON OPCIONES DE ACCESO
CREATE PROC Sesion.SPMenuUsuario(
	@_TxtToken		VARCHAR(250),
	@_IdModulo		TINYINT
)
AS
DECLARE	@_IdUsuario INT = 0 
BEGIN
	
	SELECT @_IdUsuario = Sesion.FnObtenerIdUsuario(@_TxtToken)

	SELECT 
		
		b.IdMenu, 
		a.TxtNombre, 
		a.TxtLink, 
		a.IdMenuPadre, 
		a.TxtImagen,
		b.Agregar,
		b.ModificarActualizar,
		b.Eliminar,
		b.Consultar,
		b.Imprimir,
		b.Reversar,
		b.Aprobar,
		b.Finalizar

	FROM			Sesion.TblMenus				AS a
		LEFT JOIN	Sesion.TblRolesPorMenus		AS b
		ON			a.IdMenu = b.IdMenu	
		LEFT JOIN	Sesion.TblRoles				AS c
		ON			b.IdRol = c.IdRol
		LEFT JOIN	Sesion.TblUsuariosPorRoles	AS d
		ON			d.IdRol = c.IdRol
		LEFT JOIN	Sesion.TblUsuarios			AS e
		ON			e.IdUsuario = d.IdUsuario

	WHERE			a.IntEstado = 1
			AND		a.IdModulo = @_IdModulo
			AND		b.IntEstado = 1
			AND		c.IntEstado = 1
			AND		d.IntEstado = 1
			AND		e.IntEstado = 1
			AND		d.IdUsuario = @_IdUsuario
	ORDER BY
			A.DblOrden	ASC
END

------------------------------------------------------
--------------------------------------------------
---------------------------------------------------------
--Procedimiento para agregar una especialidad

CREATE PROC RRHH.SPAgregarEspecialidad	(
											@_TxtEspecialidad		NVARCHAR(100)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdEspecialidad),0)
	FROM	RRHH.TblEspecialidades	AS	a

	SELECT @_IdUsuario = sesion.FnObtenerIdUsuario(@_TxtToken)
	
	BEGIN TRY
		INSERT INTO RRHH.TblEspecialidades	(
											IdEspecialidad
											,TxtEspecialidad
											,IdUsuario
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtEspecialidad
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 


--Procedimiento para actualizar especialidad

CREATE PROC RRHH.SPActualizarEspecialidad (
	@_IdEspecialidad SMALLINT,
	@_TxtEspecialidad NVARCHAR(100)
)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEspecialidades
			SET		TxtEspecialidad = @_TxtEspecialidad
					
					
			WHERE	IdEspecialidad = @_IdEspecialidad

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdEspecialidad
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END


CREATE PROC RRHH.SPObtenerEspecialidades
AS
BEGIN
	SELECT	a.IdEspecialidad,
			a.TxtEspecialidad,
			a.FechaIngreso
	FROM	RRHH.TblEspecialidades AS a
	WHERE a.IntEstado > 0
END --FIN



CREATE PROC RRHH.SPObtenerDatosEspecialidad(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdEspecialidad ,
			a.TxtEspecialidad
			
	FROM	RRHH.TblEspecialidades AS a
	WHERE	a.IdEspecialidad = @_IdRegistro
END


CREATE PROC RRHH.SPEliminarEspecialidad (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEspecialidades
			SET		IntEstado = 0
					
			WHERE	IdEspecialidad = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END

--PUESTOS------------------------------------------------------------------------------------------------------------------

CREATE PROC RRHH.SPAgregarPuesto	(
											@_TxtPuesto		NVARCHAR(200)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdPuesto),0)
	FROM	RRHH.TblPuestos	AS	a

	SELECT @_IdUsuario = sesion.FnObtenerIdUsuario(@_TxtToken)
	
	BEGIN TRY
		INSERT INTO RRHH.TblPuestos (
											IdPuesto
											,TxtPuesto
											,IdUsuario
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtPuesto
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 


--Procedimiento para actualizar especialidad

CREATE PROC RRHH.SPActualizarPuesto (
	@_IdRegistro SMALLINT,
	@_TxtPuesto NVARCHAR(100)
)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblPuestos
			SET		TxtPuesto = @_TxtPuesto
					
					
			WHERE	IdPuesto = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END


CREATE PROC RRHH.SPObtenerPuestos
AS
BEGIN
	SELECT	a.IdPuesto,
			a.TxtPuesto,
			a.FechaIngreso
	FROM	RRHH.TblPuestos AS a
	WHERE a.IntEstado > 0
END --FIN



CREATE PROC RRHH.SPObtenerDatosPuesto(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdPuesto,
			a.TxtPuesto
			
	FROM	RRHH.TblPuestos AS a
	WHERE	a.IdPuesto = @_IdRegistro
END

CREATE PROC RRHH.SPEliminarPuesto (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblPuestos
			SET		IntEstado = 0
					
			WHERE	IdPuesto = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END





--------------------RENGLONES-----------------------------------------------------------------------------
CREATE PROC RRHH.SPAgregarRenglon	(
											@_TxtRenglon		NVARCHAR(200)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdRenglon),0)
	FROM	RRHH.TblRenglones	AS	a

	SELECT @_IdUsuario = sesion.FnObtenerIdUsuario(@_TxtToken)
	
	BEGIN TRY
		INSERT INTO RRHH.TblRenglones(
											IdRenglon
											,TxtRenglon
											,IdUsuario
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtRenglon
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 


--Procedimiento para actualizar especialidad

CREATE PROC RRHH.SPActualizarRenglon (
	@_IdRegistro SMALLINT,
	@_TxtRenglon NVARCHAR(100)
)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblRenglones
			SET		TxtRenglon = @_TxtRenglon
					
					
			WHERE	IdRenglon = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END


CREATE PROC RRHH.SPObtenerRenglon
AS
BEGIN
	SELECT	a.IdRenglon,
			a.TxtRenglon,
			a.FechaIngreso
	FROM	RRHH.TblRenglones AS a
	WHERE a.IntEstado > 0
END --FIN



CREATE PROC RRHH.SPObtenerDatosRenglon(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdRenglon,
			a.TxtRenglon
			
	FROM	RRHH.TblRenglones AS a
	WHERE	a.IdRenglon = @_IdRegistro
END

CREATE PROC RRHH.SPEliminarRenglon (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblRenglones
			SET		IntEstado = 0
					
			WHERE	IdRenglon = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END

------------------------------------------SERVICIOS-------------------------------------
CREATE PROC RRHH.SPAgregarServicio	(
											@_TxtServicio		NVARCHAR(200)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdServicio),0)
	FROM	RRHH.TblServicios	AS	a

	SELECT @_IdUsuario = sesion.FnObtenerIdUsuario(@_TxtToken)
	
	BEGIN TRY
		INSERT INTO RRHH.TblServicios(
											IdServicio
											,TxtServicio
											,IdUsuario
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtServicio
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 


--Procedimiento para actualizar especialidad

CREATE PROC RRHH.SPActualizarServicio (
	@_IdRegistro SMALLINT,
	@_TxtServicio NVARCHAR(100)
)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblServicios
			SET		TxtServicio = @_TxtServicio
					
					
			WHERE	IdServicio = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END


CREATE PROC RRHH.SPObtenerServicio
AS
BEGIN
	SELECT	a.IdServicio,
			a.TxtServicio,
			a.FechaIngreso
	FROM	RRHH.TblServicios AS a
	WHERE a.IntEstado > 0
END --FIN



CREATE PROC RRHH.SPObtenerDatosServicio(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdServicio,
			a.TxtServicio
			
	FROM	RRHH.TblServicios AS a
	WHERE	a.IdServicio = @_IdRegistro
END

CREATE PROC RRHH.SPEliminarServicio (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblServicios
			SET		IntEstado = 0
					
			WHERE	IdServicio = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END


--------------------------------- EMPLEADOS -----------------------------
CREATE PROC RRHH.SPAgregarEmpleado	(
											@_TxtNit		NVARCHAR(15)
											,@_TxtDpi		NVARCHAR(30)
											,@_TxtNombres		NVARCHAR(50)
											,@_TxtApellidos		NVARCHAR(50)
											,@_IdPuesto			SMALLINT
											,@_IdEspecialidad	SMALLINT
											,@_IdServicio		SMALLINT
											,@_IdRenglon		SMALLINT
											,@_IdInstitucion	SMALLINT
											,@_TxtToken			NVARCHAR(250)
											
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdEmpleado),0)
	FROM	RRHH.TblEmpleados	AS	a

	SELECT @_IdUsuario = Sesion.FnObtenerIdUsuario(@_TxtToken)
	
	BEGIN TRY
		INSERT INTO RRHH.TblEmpleados(
											IdEmpleado
											,TxtNit
											,TxtDpi
											,TxtNombre
											,TxtApellidos
											,IdPuesto
											,IdEspecialidad
											,IdServicio
											,IdRenglon
											,IdInstitucion
											,IdUsuario
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtNit
											,@_TxtDpi
											,@_TxtNombres
											,@_TxtApellidos
											,@_IdPuesto
											,@_IdEspecialidad
											,@_IdServicio
											,@_IdRenglon
											,@_IdInstitucion
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 


--Procedimiento para actualizar especialidad

CREATE PROC RRHH.SPActualizarEmpleado (
											@_IdRegistro	INT
											,@_TxtNit		NVARCHAR(15)
											,@_TxtDpi		NVARCHAR(30)
											,@_TxtNombres		NVARCHAR(50)
											,@_TxtApellidos		NVARCHAR(50)
											,@_IdPuesto			SMALLINT
											,@_IdEspecialidad	SMALLINT
											,@_IdServicio		SMALLINT
											,@_IdRenglon		SMALLINT
											,@_IdInstitucion	SMALLINT
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEmpleados
			SET		TxtNit = @_TxtNit,
					TxtDpi = @_TxtDpi,
					TxtNombres = @_TxtNombres,
					TxtApellidos = @_TxtApellidos,
					IdPuesto = @_IdPuesto,
					IdEspecialidad = @_IdEspecialidad,
					IdServicio = @_IdServicio,
					IdRenglon = @_IdRenglon,
					IdInstitucion = @_IdInstitucion
					
					
			WHERE	IdEmpleado = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END


CREATE PROC RRHH.SPObtenerEmpleados
AS
BEGIN
	SELECT	a.IdEmpleado,
			a.TxtNit,
			a.TxtDpi,
			a.TxtNombres,
			a.TxtApellidos,
			b.TxtPuesto,
			c.TxtEspecialidad,
			d.TxtServicio,
			e.TxtRenglon,
			f.TxtNombre AS TxtInstitucion,
			a.FechaIngreso
	FROM		RRHH.TblEmpleados	AS a
	LEFT JOIN	RRHH.TblPuestos		AS b
	ON	b.IdPuesto = a.IdPuesto
	LEFT JOIN	RRHH.TblEspecialidades	AS c
	ON	c.IdEspecialidad = a.IdEspecialidad
	LEFT JOIN	RRHH.TblServicios	AS d
	ON	d.IdServicio = a.IdServicio
	LEFT JOIN	RRHH.TblRenglones	AS e
	ON	e.IdRenglon = a.IdRenglon
	LEFT JOIN	Sistema.TblInstituciones AS f
	ON f.IdInstitucion = a.IdInstitucion
	WHERE a.IntEstado > 0
END --FIN



CREATE PROC RRHH.SPObtenerDatosEmpleado(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdEmpleado,
			a.TxtNit,
			a.TxtDpi,
			a.TxtNombres,
			a.TxtApellidos,
			a.IdPuesto,
			a.IdEspecialidad,
			a.IdServicio,
			a.IdRenglon,
			a.IdInstitucion			
	FROM	RRHH.TblEmpleados AS a
	WHERE	a.IdEmpleado = @_IdRegistro
END

CREATE PROC RRHH.SPEliminarEmpleado (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEmpleados
			SET		IntEstado = 0
					
			WHERE	IdEmpleado = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END

-----------------------------------------------------------------------------------------------------
/*

	Author: ILMER ALDANA
	Fecha: 11/10/2020

*/

CREATE PROC Sesion.SPVerificarVigenciaToken (
	@_TxtToken NVARCHAR(250)
)
AS
DECLARE @_EstadoToken TINYINT = 0
BEGIN
	--0 = Expirado, 1 = Vigente
	SELECT @_EstadoToken = Sesion.FnVerificarVigenciaToken(@_TxtToken)

	SELECT EstadoToken = @_EstadoToken
END


--*************FACTORES******************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR FACTOR ----------------------
CREATE PROC RRHH.SPAgregarFactor	(
											@_TxtFactor		NVARCHAR(200)
											,@_TxtDecripcion	NVARCHAR(MAX)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdFactor),0)
	FROM	RRHH.TblFactores	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblFactores(
											IdFactor
											,TxtFactor
											,TxtDescripcion
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtFactor
											,@_TxtDecripcion
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR FACTORES---------------------------

CREATE PROC RRHH.SPActualizarFactor (
											@_IdRegistro	INT
											,@_TxtFactor	NVARCHAR(200)
											,@_TxtDescripcion	NVARCHAR(MAX)
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblFactores
			SET		TxtFactor = @_TxtFactor,
					TxtDescripcion = @_TxtDescripcion		
					
			WHERE	IdFactor = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

--------------OBTENER FACTORES----------------------

CREATE PROC RRHH.SPObtenerFactores
AS
BEGIN
	SELECT	a.IdFactor,
			a.TxtFactor,
			a.TxtDescripcion,
			CONCAT(b.TxtNombres,' ', b.TxtApellidos),
			a.FechaIngreso
	FROM		RRHH.TblFactores	AS a
	LEFT JOIN	Sesion.TblUsuarios		AS b
	ON	b.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS FACTOR---------------------------

CREATE PROC RRHH.SPObtenerDatosFactor(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdFactor,
			a.TxtFactor,
			a.TxtDescripcion						
	FROM	RRHH.TblFactores AS a
	WHERE	a.IdFactor = @_IdRegistro
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR FACTOR ------------------------

CREATE PROC RRHH.SPEliminarFactor (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblFactores
			SET		IntEstado = 0
					
			WHERE	IdFactor = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END


--*************SUBFACTORES******************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR SUBFACTOR ----------------------
CREATE PROC RRHH.SPAgregarSubFactor	(
											@_TxtSubFactor		NVARCHAR(200)
											,@_TxtDecripcion	NVARCHAR(MAX)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdSubFactor),0)
	FROM	RRHH.TblSubFactores	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblSubFactores(
											IdSubFactor
											,TxtSubFactor
											,TxtDescripcion
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtSubFactor
											,@_TxtDecripcion
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR SUBFACTOR---------------------------

CREATE PROC RRHH.SPActualizarSubFactor (
											@_IdRegistro	INT
											,@_TxtSubFactor	NVARCHAR(200)
											,@_TxtDescripcion	NVARCHAR(MAX)
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblSubFactores
			SET		TxtSubFactor = @_TxtSubFactor,
					TxtDescripcion = @_TxtDescripcion		
					
			WHERE	IdSubFactor = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

--------------OBTENER SUBFACTORES----------------------

CREATE PROC RRHH.SPObtenerSubFactores
AS
BEGIN
	SELECT	a.IdSubFactor,
			a.TxtSubFactor,
			a.TxtDescripcion,
			CONCAT(b.TxtNombres,' ', b.TxtApellidos),
			a.FechaIngreso
	FROM		RRHH.TblSubFactores	AS a
	LEFT JOIN	Sesion.TblUsuarios		AS b
	ON	b.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS SUBFACTOR---------------------------

CREATE PROC RRHH.SPObtenerDatosSubFactor(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdSubFactor,
			a.TxtSubFactor,
			a.TxtDescripcion						
	FROM	RRHH.TblSubFactores AS a
	WHERE	a.IdSubFactor = @_IdRegistro
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR SUBFACTOR ------------------------

CREATE PROC RRHH.SPEliminarSubFactor (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblSubFactores
			SET		IntEstado = 0
					
			WHERE	IdSubFactor = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END


--*************ESCALAS DE CALIFICACION******************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR ESCALAS DE CALIFICACION ----------------------
CREATE PROC RRHH.SPAgregarEscalaDeCalificacion	(
											@_TxtEscalaDeCalificacion	NVARCHAR(250)
											,@_DblPunteo		DECIMAL(5,2)
											,@_TxtDecripcion	NVARCHAR(MAX)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdEscalaDeCalificacion),0)
	FROM	RRHH.TblEscalasDeCalificacion	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblEscalasDeCalificacion(
											IdEscalaDeCalificacion
											,TxtEscalaDeCalificacion
											,DblPunteo
											,TxtDescripcion
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtEscalaDeCalificacion
											,@_DblPunteo
											,@_TxtDecripcion
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR ESCALA DE CALIFICACION---------------------------

CREATE PROC RRHH.SPActualizarEscalaDeCalificacion (
											@_IdRegistro	INT
											,@_TxtEscalaDeCalificacion	NVARCHAR(250)
											,@_DblPunteo		DECIMAL(5,2)
											,@_TxtDescripcion	NVARCHAR(MAX)
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEscalasDeCalificacion
			SET		TxtEscalaDeCalificacion = @_TxtEscalaDeCalificacion,
					DblPunteo = @_DblPunteo,
					TxtDescripcion = @_TxtDescripcion		
					
			WHERE	IdEscalaDeCalificacion = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

--------------OBTENER ESCALAS DE CALIFICACION----------------------

CREATE PROC RRHH.SPObtenerEscalasDeCalificacion
AS
BEGIN
	SELECT	a.IdEscalaDeCalificacion,
			a.TxtEscalaDeCalificacion,
			a.DblPunteo,
			a.TxtDescripcion,
			CONCAT(b.TxtNombres,' ', b.TxtApellidos),
			a.FechaIngreso
	FROM		RRHH.TblEscalasDeCalificacion	AS a
	LEFT JOIN	Sesion.TblUsuarios		AS b
	ON	b.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS ESCALA DE CALIFICACION---------------------------

CREATE PROC RRHH.SPObtenerDatosEscalaDeCalificacion(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdEscalaDeCalificacion,
			a.TxtEscalaDeCalificacion,
			a.DblPunteo,
			a.TxtDescripcion						
	FROM	RRHH.TblEscalasDeCalificacion AS a
	WHERE	a.IdEscalaDeCalificacion = @_IdRegistro
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR ESCALA DE CALIFICACION ------------------------

CREATE PROC RRHH.SPEliminarEscalaDeCalificacion (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEscalasDeCalificacion
			SET		IntEstado = 0
					
			WHERE	IdEscalaDeCalificacion = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END

--*************TIPOS DE EVALUACIONES******************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR TIPO DE EVALUACION ----------------------
CREATE PROC RRHH.SPAgregarTipoDeEvaluacion	(
											@_TxtTipoDeEvaluacion		NVARCHAR(150)
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdTipoDeEvaluacion),0)
	FROM	RRHH.TblTiposDeEvaluaciones	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblTiposDeEvaluaciones(
											IdTipoDeEvaluacion
											,TxtTipoDeEvaluacion
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_TxtTipoDeEvaluacion
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR TIPO DE EVALUACION---------------------------

CREATE PROC RRHH.SPActualizarTipoDeEvaluacion (
											@_IdRegistro	INT
											,@_TxtTipoDeEvaluacion	NVARCHAR(150)
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblTiposDeEvaluaciones
			SET		TxtTipoDeEvaluacion = @_TxtTipoDeEvaluacion	
					
			WHERE	IdTipoDeEvaluacion = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

--------------OBTENER TIPOS DE EVALUACIONES----------------------

CREATE PROC RRHH.SPObtenerTiposDeEvaluaciones
AS
BEGIN
	SELECT	a.IdTipoDeEvaluacion,
			a.TxtTipoDeEvaluacion,
			CONCAT(b.TxtNombres,' ', b.TxtApellidos),
			a.FechaIngreso
	FROM		RRHH.TblTiposDeEvaluaciones	AS a
	LEFT JOIN	Sesion.TblUsuarios		AS b
	ON	b.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS TIPO DE EVALUACION---------------------------

CREATE PROC RRHH.SPObtenerDatosTipoDeEvaluacion(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdTipoDeEvaluacion,
			a.TxtTipoDeEvaluacion						
	FROM	RRHH.TblTiposDeEvaluaciones AS a
	WHERE	a.IdTipoDeEvaluacion = @_IdRegistro
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR TIPO DE EVALUACION------------------------

CREATE PROC RRHH.SPEliminarTipoDeEvaluacion (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblTiposDeEvaluaciones
			SET		IntEstado = 0
					
			WHERE	IdTipoDeEvaluacion = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END

--*************EVALUACIONES ENCABEZADO******************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR EVALUACION ENCABEZADO ----------------------
CREATE PROC RRHH.SPAgregarEvaluacionEncabezado	(
											@_IdTipoDeEvaluacion TINYINT
											,@_Anio				SMALLINT
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdEvaluacionEncabezado),0)
	FROM	RRHH.TblEvaluacionesEncabezado	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblEvaluacionesEncabezado(
											IdEvaluacionEncabezado
											,IdTipoDeEvaluacion
											,Anio
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_IdTipoDeEvaluacion
											,@_Anio
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR EVALUACION ENCABEZADO---------------------------

CREATE PROC RRHH.SPActualizarEvaluacionEncabezado (
											@_IdRegistro	INT
											,@_IdTipoDeEvaluacion	TINYINT
											,@_Anio	SMALLINT
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesEncabezado
			SET		IdEvaluacionEncabezado = @_IdTipoDeEvaluacion,
					Anio = @_Anio		
					
			WHERE	IdEvaluacionEncabezado = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

--------------OBTENER EVALUACIONES ENCABEZADO----------------------

CREATE PROC RRHH.SPObtenerEvaluacionesEncabezado
AS
BEGIN
	SELECT	a.IdEvaluacionEncabezado,
			b.TxtTipoDeEvaluacion,
			a.Anio,
			CONCAT(c.TxtNombres,' ', c.TxtApellidos),
			a.FechaIngreso
	FROM		RRHH.TblEvaluacionesEncabezado	AS a
	LEFT JOIN	RRHH.TblTiposDeEvaluaciones AS b
	ON b.IdTipoDeEvaluacion = a.IdTipoDeEvaluacion
	LEFT JOIN	Sesion.TblUsuarios		AS c
	ON	c.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS EVALUACION ENCABEZADO---------------------------

CREATE PROC RRHH.SPObtenerDatosEvaluacionEncabezado(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdEvaluacionEncabezado,
			a.IdTipoDeEvaluacion,
			a.Anio						
	FROM	RRHH.TblEvaluacionesEncabezado AS a
	WHERE	a.IdEvaluacionEncabezado = @_IdRegistro
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR EVALUACION ENCABEZADO ------------------------

CREATE PROC RRHH.SPEliminarEvaluacionEncabezado (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesEncabezado
			SET		IntEstado = 0
					
			WHERE	IdEvaluacionEncabezado = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END


--*************EVALUACION DETALLE******************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR EVALUACION DETALLE ----------------------
CREATE PROC RRHH.SPAgregarEvaluacionDetalle	(
											@_IdEvaluacionEncabezado	INT
											,@_IdFactor					SMALLINT
											,@_IdSubFactor				SMALLINT
											,@_TxtToken					NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdEvaluacionDetalle),0)
	FROM	RRHH.TblEvaluacionesDetalle	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblEvaluacionesDetalle(
											IdEvaluacionDetalle
											,IdEvaluacionEncabezado
											,IdFactor
											,IdSubFactor
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_IdEvaluacionEncabezado
											,@_IdFactor
											,@_IdSubFactor
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR EVALUACION DETALLE---------------------------

CREATE PROC RRHH.SPActualizarEvaluacionDetalle (
											@_IdRegistro				INT
											,@_IdEvaluacionEncabezado	INT
											,@_IdFactor					SMALLINT
											,@_IdSubFactor				SMALLINT
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAND
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesDetalle
			SET		IdEvaluacionEncabezado = @_IdEvaluacionEncabezado,
					IdFactor = @_IdFactor,
					IdSubFactor = @_IdSubFactor
					
			WHERE	IdEvaluacionDetalle = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA 
	Fecha: 17/10/2020

*/

--------------OBTENER EVALUACIONES DETALLE----------------------

CREATE PROC RRHH.SPObtenerEvaluacionesDetalle
AS
BEGIN
	SELECT	a.IdEvaluacionDetalle,
			b.IdEvaluacionEncabezado,
			c.TxtFactor,
			d.TxtSubFactor,
			CONCAT(e.TxtNombres,' ', e.TxtApellidos),
			a.FechaIngreso
	FROM		RRHH.TblEvaluacionesDetalle	AS a
	LEFT JOIN	RRHH.TblEvaluacionesEncabezado AS b
	ON	b.IdEvaluacionEncabezado = a.IdEvaluacionEncabezado
	LEFT JOIN	RRHH.TblFactores AS c
	ON	c.IdFactor = a.IdFactor
	LEFT JOIN	RRHH.TblSubFactores AS d
	ON	d.IdSubFactor = a.IdSubFactor
	LEFT JOIN	Sesion.TblUsuarios		AS e
	ON	e.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS EVALUACION DETALLE---------------------------

CREATE PROC RRHH.SPObtenerDatosEvaluacionDetalle(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdEvaluacionDetalle,
			a.IdEvaluacionEncabezado,
			a.IdFactor,
			a.IdSubFactor					
	FROM	RRHH.TblEvaluacionesDetalle AS a
	WHERE	a.IdEvaluacionDetalle = @_IdRegistro
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR EVALUACION DETALLE ------------------------

CREATE PROC RRHH.SPEliminarEvaluacionDetalle (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesDetalle
			SET		IntEstado = 0
					
			WHERE	IdEvaluacionDetalle = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END

--************ EVALUACIONES APLICADAS ENCABEZADA *****************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR EVALUACION APLICADA ENCABEZADO ----------------------
CREATE PROC RRHH.SPAgregarEvaluacionAplicadaEncabezado	(
					
											@_IdInstitucion				SMALLINT
											,@_IdEvaluacionEncabezado	INT
											,@_IdEmpleado				INT
											,@_FechaDeAplicacion		DATE
											,@_FechaInicial				DATE
											,@_FechaFinal				DATE
											,@_DblPunteoFinal			DECIMAL(5,2)
											,@_TxtObservacionesDeJefe	NVARCHAR(MAX)
											,@_TxtObservacionesDelEmpleado NVARCHAR(MAX)
											,@_IntNecesitaPlanDeMejora	TINYINT
											,@_TxtToken					NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdEvaluacionAplicadaEncabezado),0)
	FROM	RRHH.TblEvaluacionesAplicadasEncabezado	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblEvaluacionesAplicadasEncabezado(

											IdEvaluacionAplicadaEncabezado
											,IdInstitucion
											,IdEvaluacionEncabezado
											,IdEmpleado
											,FechaDeAplicacion
											,FechaInicial
											,FechaFinal
											,DblPunteoTotal
											,TxtObservacionesDeJefe
											,TxtObservacionesDelEmpleado
											,IntNecesitaPlanDeMejora
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_IdInstitucion
											,@_IdEvaluacionEncabezado
											,@_IdEmpleado
											,@_FechaDeAplicacion
											,@_FechaInicial
											,@_FechaFinal
											,@_DblPunteoFinal
											,@_TxtObservacionesDeJefe
											,@_TxtObservacionesDelEmpleado
											,@_IntNecesitaPlanDeMejora
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR EVALULACION APLICADA ENCABEZADO---------------------------

CREATE PROC RRHH.SPActualizarEvaluacionAplicadaEncabezado (

											@_IdRegistro				INT
											,@_IdInstitucion			SMALLINT
											,@_IdEvaluacionEncabezado	INT
											,@_IdEmpleado				INT
											,@_FechaDeAplicacion		DATE
											,@_FechaInicial				DATE
											,@_FechaFinal				DATE
											,@_DblPunteoFinal			DECIMAL(5,2)
											,@_TxtObservacionesDeJefe	NVARCHAR(MAX)
											,@_TxtObservacionesDelEmpleado NVARCHAR(MAX)
											,@_IntNecesitaPlanDeMejora	TINYINT
											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesAplicadasEncabezado
			SET		IdInstitucion = @_IdInstitucion,
					IdEvaluacionEncabezado = @_IdEvaluacionEncabezado,
					IdEmpleado = @_IdEmpleado,
					FechaDeAplicacion = @_FechaDeAplicacion,
					FechaInicial = @_FechaInicial,
					FechaFinal = @_FechaFinal,
					DblPunteoTotal = @_DblPunteoFinal,
					TxtObservacionesDeJefe = @_TxtObservacionesDeJefe,
					TxtObservacionesDelEmpleado = @_TxtObservacionesDelEmpleado,
					IntNecesitaPlanDeMejora = @_IntNecesitaPlanDeMejora	
					
			WHERE	IdEvaluacionAplicadaEncabezado = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

--------------OBTENER EVALUACIONES APLICADAS ENCABEZADO----------------------

CREATE PROC RRHH.SPObtenerEvaluacionesAplicadasEncabezado
AS
BEGIN
	SELECT	a.IdEvaluacionAplicadaEncabezado,
			b.TxtNombre,
			c.IdEvaluacionEncabezado,
			CONCAT(d.TxtNombres,' ',d.TxtApellidos),
			a.FechaDeAplicacion,
			a.FechaInicial,
			a.FechaFinal,
			a.DblPunteoTotal,
			a.TxtObservacionesDeJefe,
			a.TxtObservacionesDelEmpleado,
			a.IntNecesitaPlanDeMejora,
			CONCAT(e.TxtNombres,' ', e.TxtApellidos),
			a.FechaIngreso
		
	FROM		RRHH.TblEvaluacionesAplicadasEncabezado	AS a
	LEFT JOIN	Sistema.TblInstituciones AS b
	ON	b.IdInstitucion = a.IdInstitucion
	LEFT JOIN	RRHH.TblEvaluacionesEncabezado AS c
	ON	c.IdEvaluacionEncabezado = a.IdEvaluacionEncabezado
	LEFT JOIN	RRHH.TblEmpleados AS d
	ON	d.IdEmpleado = a.IdEmpleado
	LEFT JOIN	Sesion.TblUsuarios		AS e
	ON	e.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS EVALUACION APLICADA ENCABEZADO---------------------------

CREATE PROC RRHH.SPObtenerDatosEvaluacionAplicadaEncabezado(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdEvaluacionAplicadaEncabezado,
			a.IdInstitucion,
			a.IdEvaluacionEncabezado,
			a.IdEmpleado,
			a.FechaDeAplicacion,
			a.FechaInicial,
			a.FechaFinal,
			a.DblPunteoTotal,
			a.TxtObservacionesDeJefe,
			a.TxtObservacionesDelEmpleado,
			a.IntNecesitaPlanDeMejora
								
	FROM	RRHH.TblEvaluacionesAplicadasEncabezado AS a
	WHERE	a.IdEvaluacionAplicadaEncabezado = @_IdRegistro
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR EVALUACION APLICADA ENCABEZADO ------------------------

CREATE PROC RRHH.SPEliminarEvaluacionAplicadaEncabezado (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesAplicadasEncabezado
			SET		IntEstado = 0
					
			WHERE	IdEvaluacionAplicadaEncabezado = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END


--*************EVALUACIONES APLICADAS DETALLE******************

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

------------------AGREGAR EVALUACION APLICADA DETALLE ----------------------
CREATE PROC RRHH.SPAgregarEvaluacionAplicadaDetalle	(
											@_IdEvaluacionAplicadaEncabezado	INT
											,@_IdEvaluacionDetalle INT
											,@_IdEscalaDeCalificacion TINYINT
											,@_TxtToken			NVARCHAR(250)
									)	
AS
DECLARE @_FilasAfectadas		TINYINT
		,@_Resultado			SMALLINT
		,@_UltimoId				SMALLINT
		,@_IdUsuario			INT
BEGIN
BEGIN TRAN
	--OBTENGO EL ULTIMO ID GUARDADO EN LA TABLA
	SELECT	@_UltimoId			=	ISNULL(MAX(a.IdEvaluacionAplicadaDetalle),0)
	FROM	RRHH.TblEvaluacionesAplicadasDetalle	AS	a

	SELECT @_IdUsuario			=	b.IdUsuario
	FROM	Sesion.TblTokens	AS	b
	WHERE b.TxtToken			=	@_TxtToken
	
	BEGIN TRY
		INSERT INTO RRHH.TblEvaluacionesAplicadasDetalle(

											IdEvaluacionAplicadaDetalle
											,IdEvaluacionAplicadaEncabezado
											,IdEvaluacionDetalle
											,IdEscalaDeCalificacion
											,IdUsuario
											
										)
		VALUES							(
											@_UltimoId + 1
											,@_IdEvaluacionAplicadaEncabezado
											,@_IdEvaluacionDetalle
											,@_IdEscalaDeCalificacion
											,@_IdUsuario
										)
		SET @_FilasAfectadas			=	@@ROWCOUNT
	END TRY
	BEGIN CATCH
		SET @_FilasAfectadas			=	0
	END CATCH		

--DETERMINAR SI SE REALIZO CORRECTAMENTE LA TRANSACCION ANTERIOR
IF (@_FilasAfectadas > 0)
		BEGIN
			SET @_Resultado			=	@_UltimoId + 1
			COMMIT
		END
	ELSE
		BEGIN
			SET @_Resultado			=	0
			ROLLBACK
		END
	--DEVOLVER RESULTADO: EL ULTIMO ID QUE UTILIZAR� M�S ADELANTE
	SELECT Resultado				=	@_Resultado
END --FIN 

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------ACTUALIZAR EVALUACION APLICADA DETALLE---------------------------

CREATE PROC RRHH.SPActualizarEvaluacionAplicadaDetalle (

											@_IdRegistro	INT
											,@_IdEvaluacionAplicadaEncabezado	INT
											,@_IdEvaluacionDetalle INT
											,@_IdEscalaDeCalificacion TINYINT

											)

AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesAplicadasDetalle
			SET		IdEvaluacionAplicadaEncabezado = @_IdEvaluacionAplicadaEncabezado,
					IdEvaluacionDetalle = @_IdEvaluacionDetalle,
					IdEscalaDeCalificacion = @_IdEscalaDeCalificacion		
					
			WHERE	IdEvaluacionAplicadaDetalle = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END

	SELECT Resultado  = @_Resultado
END

/*

	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

--------------OBTENER EVALUACIONES APLICADAS DETALLE----------------------

CREATE PROC RRHH.SPObtenerEvaluacionesAplicadasDetalle
AS
BEGIN
	SELECT	a.IdEvaluacionAplicadaDetalle,
			b.IdEvaluacionAplicadaEncabezado,
			c.IdEvaluacionDetalle,
			d.TxtEscalaDeCalificacion,
			CONCAT(e.TxtNombres,' ', e.TxtApellidos),
			a.FechaIngreso

	FROM		RRHH.TblEvaluacionesAplicadasDetalle	AS a
	LEFT JOIN	RRHH.TblEvaluacionesAplicadasEncabezado AS b
	ON	b.IdEvaluacionAplicadaEncabezado = a.IdEvaluacionAplicadaEncabezado
	LEFT JOIN	RRHH.TblEvaluacionesDetalle AS c
	ON	c.IdEvaluacionDetalle = a.IdEvaluacionDetalle
	LEFT JOIN	RRHH.TblEscalasDeCalificacion AS d
	ON	d.IdEscalaDeCalificacion = a.IdEscalaDeCalificacion
	LEFT JOIN	Sesion.TblUsuarios		AS e
	ON	e.IdUsuario = a.IdUsuario
	
	WHERE a.IntEstado > 0
END --FIN


/*
	
	Author: ILMER ALDANA
	Fecha: 17/10/2020

*/

-----------------OBTENER DATOS EVALULACION APLICADA DETALLE---------------------------

CREATE PROC RRHH.SPObtenerDatosEvaluacionAplicadaDetalle(
	@_IdRegistro INT
)

AS
BEGIN
	SELECT	a.IdEvaluacionAplicadaDetalle,
			a.IdEvaluacionAplicadaEncabezado,
			a.IdEvaluacionDetalle,
			a.IdEscalaDeCalificacion						
	FROM	RRHH.TblEvaluacionesAplicadasDetalle AS a
	WHERE	a.IdEvaluacionAplicadaDetalle = @_IdRegistro
END

/*

	Author: ILLMER ALDANA
	Fecha: 17/10/2020

*/

-------------------- ELIMINAR EVALUACION APLICADA DETALLE------------------------

CREATE PROC RRHH.SPEliminarEvaluacionAplicadaDetalle (
	@_IdRegistro INT
)
AS
DECLARE 
	@_FilasAfectadas	TINYINT,
	@_Resultado			INT

BEGIN
	BEGIN TRAN
		BEGIN TRY
			UPDATE	RRHH.TblEvaluacionesAplicadasDetalle
			SET		IntEstado = 0
					
			WHERE	IdEvaluacionAplicadaDetalle = @_IdRegistro

			SET		@_FilasAfectadas = @@ROWCOUNT
		END TRY
		BEGIN CATCH
			SET		@_FilasAfectadas = 0
		END CATCH

		IF(@_FilasAfectadas > 0)
			BEGIN
				SET @_Resultado = @_IdRegistro
				COMMIT
			END
		ELSE
			BEGIN
				SET @_Resultado = 0
			ROLLBACK
		END
	SELECT Resultado  = @_Resultado
END