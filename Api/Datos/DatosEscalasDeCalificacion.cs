﻿using Entidades;
using System.Data;
using System.Data.SqlClient;

namespace Datos
{
    public class DatosEscalasDeCalificacion
    {
        private static readonly Funciones Funciones = new Funciones();
        private static readonly int VigenciaEnMinutos = 30;
        private static DataTable DT = new DataTable();
        private static int Estado = 0;

        public static DataTable AgregarEscalaDeCalificacion(EntidadEscalasDeCalificacion Entidad)
        {
            Estado = Funciones.ObtenerEstadoToken(Entidad.TxtToken);
            DT.Clear();

            // 0 = Expirado , 1 = Vigente
            if (Estado == 1)
            {

                SqlCommand Comando = Conexion.CrearComandoProc("RRHH.SPAgregarEscalaDeCalificacion");
                Comando.Parameters.AddWithValue("@_TxtEscalaDeCalificacion", Entidad.TxtEscalaDeCalificacion);
                Comando.Parameters.AddWithValue("@_DblPunteo", Entidad.DblPunteo);
                Comando.Parameters.AddWithValue("@_TxtDescripcion", Entidad.TxtDescripcion);
                Comando.Parameters.AddWithValue("@_TxtToken", Entidad.TxtToken);

                DT = Conexion.EjecutarComandoSelect(Comando);
                DT = Funciones.AgregarEstadoToken(DT, Estado.ToString());
            }
            else
            {
                DT = Funciones.AgregarEstadoToken(DT, "0");
            }

            return DT;
        }

        public static DataTable ObtenerEscalasDeCalificacion(EntidadEscalasDeCalificacion Entidad)
        {

            Estado = Funciones.ObtenerEstadoToken(Entidad.TxtToken);
            DT.Clear();

            if (Estado == 1)
            {
                SqlCommand Comando = Conexion.CrearComandoProc("RRHH.SPObtenerEscalasDeCalificacion");

                DT = Conexion.EjecutarComandoSelect(Comando);
                DT = Funciones.AgregarEstadoToken(DT, Estado.ToString());
            }
            else
            {
                DT = Funciones.AgregarEstadoToken(DT, "0");
            }

            return DT;

        }

        public static DataTable ObtenerDatosEscalaDeCalificacion(EntidadEscalasDeCalificacion Entidad)
        {

            Estado = Funciones.ObtenerEstadoToken(Entidad.TxtToken);
            DT.Clear();

            if (Estado == 1)
            {

                SqlCommand Comando = Conexion.CrearComandoProc("RRHH.SPObtenerDatosEscalaDeCalificacion");
                Comando.Parameters.AddWithValue("@_IdRegistro", Entidad.IdEscalaDeCalificacion);

                DT = Conexion.EjecutarComandoSelect(Comando);
                DT = Funciones.AgregarEstadoToken(DT, Estado.ToString());
            }
            else
            {
                DT = Funciones.AgregarEstadoToken(DT, "0");
            }

            return DT;
        }

        public static DataTable EliminarEscalaDeCalificacion(EntidadEscalasDeCalificacion Entidad)
        {
            Estado = Funciones.ObtenerEstadoToken(Entidad.TxtToken);
            DT.Clear();

            if (Estado == 1)
            {

                SqlCommand Comando = Conexion.CrearComandoProc("RRHH.SPEliminarEscalaDeCalificacion");
                Comando.Parameters.AddWithValue("@_IdRegistro", Entidad.IdEscalaDeCalificacion);

                DT = Conexion.EjecutarComandoSelect(Comando);
                DT = Funciones.AgregarEstadoToken(DT, Estado.ToString());
            }
            else
            {
                DT = Funciones.AgregarEstadoToken(DT, "0");
            }

            return DT;

        }

        public static DataTable ActualizarEscalaDeCalificacion(EntidadEscalasDeCalificacion Entidad)
        {
            Estado = Funciones.ObtenerEstadoToken(Entidad.TxtToken);
            DT.Clear();

            if (Estado == 1)
            {

                SqlCommand Comando = Conexion.CrearComandoProc("RRHH.SPActualizarEscalaDeCalificacion");
                Comando.Parameters.AddWithValue("@_IdRegistro", Entidad.IdEscalaDeCalificacion);
                Comando.Parameters.AddWithValue("@_TxtEscalaDeCalificacion", Entidad.TxtEscalaDeCalificacion);
                Comando.Parameters.AddWithValue("@_DblPunteo", Entidad.DblPunteo);
                Comando.Parameters.AddWithValue("@_TxtDescripcion", Entidad.TxtDescripcion);

                DT = Conexion.EjecutarComandoSelect(Comando);
                DT = Funciones.AgregarEstadoToken(DT, Estado.ToString());
            } else
            {
                DT = Funciones.AgregarEstadoToken(DT, "0");
            }

            return DT;
        }
        
    }
}
