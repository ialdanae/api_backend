﻿
using System.Dynamic;

namespace Entidades
{
    public class EntidadTokens
    {
        public int IntResultado { get; set; }
        public int IdToken { get; set; }
        public string TxtToken { get; set; }
        public int VigenciaEnMinutos { get; set; }
        public int IdUsuario { get; set; }
        public string FechaIngreso { get; set; }
        public int IntEstado { get; set; }
        public int IdInstitucion { get; set; }

        public int IdModulo {get; set;}

    }
}
