﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadFactores : EntidadTokens
    {
        public int IdFactor { get; set; }
        public string TxtFactor { get; set; }
        public string TxtDescripcion { get; set; }

    }
}
