﻿
namespace Entidades
{
    public class EntidadEscalasDeCalificacion: EntidadTokens
    {
        public int IdEscalaDeCalificacion { get; set; }
        public string TxtEscalaDeCalificacion { get; set; }
        public double DblPunteo { get; set; }
        public string TxtDescripcion { get; set; }

    }
}
