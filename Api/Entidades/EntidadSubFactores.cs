﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class EntidadSubFactores : EntidadTokens
    {
        public int IdSubFactor { get; set; }
        public string TxtSubFactor { get; set; }
        public string TxtDescripcion { get; set; }

    }
}
