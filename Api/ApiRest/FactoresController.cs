﻿using System.Data;
using System.Web.Http;
using Datos;
using Entidades;

namespace ApiRest
{
    public class FactoresController : ApiController
    {
        
        [HttpPost]
        [Route("api/AgregarFactor")]
        public DataTable AgregarFactor(EntidadFactores entidad)
        {
            return DatosFactores.AgregarFactor(entidad);
        }

        [HttpPost]
        [Route("api/ObtenerFactores")]
        public DataTable ObtenerFactores(EntidadFactores Entidad)
        {
            return DatosFactores.ObtenerFactores(Entidad);
        }

        [HttpPost]
        [Route("api/ObtenerDatosFactor")]
        public DataTable ObtenerDatosFactor(EntidadFactores entidad)
        {
            return DatosFactores.ObtenerDatosFactor(entidad);
        }

        [HttpPost]
        [Route("api/EliminarFactor")]
        public DataTable EliminarFactor(EntidadFactores entidad)
        {
            return DatosFactores.EliminarFactor(entidad);
        }

        [HttpPost]
        [Route("api/ActualizarFactor")]
        public DataTable ActualizarFactor(EntidadFactores entidad)
        {
            return DatosFactores.ActualizarFactor(entidad);
        }

    }
}