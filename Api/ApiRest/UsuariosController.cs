﻿using System.Data;
using System.Web.Http;
using Datos;
using Entidades;

namespace ApiRest
{
    public class UsuariosController : ApiController
    {
        [HttpPost]
        [Route("api/ObtenerUsuarios")]
        public DataTable ObtenerUsuarios(EntidadUsuarios Entidad)
        {
            return DatosUsuarios.ObtenerUsuarios(Entidad);
        }

        [HttpPost]
        [Route("api/AgregarUsuario")]
        public DataTable AgregarUsuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.AgregarUsuario(entidad);
        }        

        [HttpPost]
        [Route("api/ObtenerDatosUsuario")]
        public DataTable ObtenerDatosUsuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.ObtenerDatosUsuario(entidad);
        }

        [HttpPost]
        [Route("api/EliminarUsuario")]
        public DataTable EliminarUsuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.EliminarUsuario(entidad);
        }

        [HttpPost]
        [Route("api/ActualizarUsuario")]
        public DataTable ActualizarUsuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.ActualizarUsuario(entidad);
        }

        [HttpPost]
        [Route("api/InicioDeSesion")]
        public DataTable InicioDeSesion(EntidadUsuarios entidad)
        {
            return DatosUsuarios.InicioDeSesion(entidad);
        }

        [HttpPost]
        [Route("api/MenuUsuario")]
        public DataTable MenuUsuario(EntidadUsuarios entidad)
        {
            return DatosUsuarios.MenuUsuario(entidad);
        }

        [HttpPost]
        [Route("api/VerificarVigenciaToken")]
        public int VerificarVigenciaToken(EntidadUsuarios entidad)
        {
            return Funciones.VerificarVigenciaToken(entidad);
        }
    }
}