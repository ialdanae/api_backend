﻿using System.Data;
using System.Web.Http;
using Datos;
using Entidades;

namespace ApiRest
{
    public class SubFactoresController : ApiController
    {
        
        [HttpPost]
        [Route("api/AgregarSubFactor")]
        public DataTable AgregarSubFactor(EntidadSubFactores entidad)
        {
            return DatosSubFactores.AgregarSubFactor(entidad);
        }

        [HttpPost]
        [Route("api/ObtenerSubFactores")]
        public DataTable ObtenerSubFactores(EntidadSubFactores Entidad)
        {
            return DatosSubFactores.ObtenerSubFactores(Entidad);
        }

        [HttpPost]
        [Route("api/ObtenerDatosSubFactor")]
        public DataTable ObtenerDatosSubFactor(EntidadSubFactores entidad)
        {
            return DatosSubFactores.ObtenerDatosSubFactor(entidad);
        }

        [HttpPost]
        [Route("api/EliminarSubFactor")]
        public DataTable EliminarSubFactor(EntidadSubFactores entidad)
        {
            return DatosSubFactores.EliminarSubFactor(entidad);
        }

        [HttpPost]
        [Route("api/ActualizarSubFactor")]
        public DataTable ActualizarSubFactor(EntidadSubFactores entidad)
        {
            return DatosSubFactores.ActualizarSubFactor(entidad);
        }

    }
}