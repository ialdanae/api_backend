﻿using System.Data;
using System.Web.Http;
using Datos;
using Entidades;

namespace ApiRest
{
    public class EscalasDeCalificacionesController : ApiController
    {
        [HttpPost]
        [Route("api/ObtenerEscalasDeCalificacion")]
        public DataTable ObtenerEscalasDeCalificacion(EntidadEscalasDeCalificacion Entidad)
        {
            return DatosEscalasDeCalificacion.ObtenerEscalasDeCalificacion(Entidad);
        }

        [HttpPost]
        [Route("api/AgregarEscalaDeCalificacion")]
        public DataTable AgregarEscalaDeCalificacion(EntidadEscalasDeCalificacion entidad)
        {
            return DatosEscalasDeCalificacion.AgregarEscalaDeCalificacion(entidad);
        }        

        [HttpPost]
        [Route("api/ObtenerDatosEscalaDeCalificacion")]
        public DataTable ObtenerDatosEscalaDeCalificacion(EntidadEscalasDeCalificacion entidad)
        {
            return DatosEscalasDeCalificacion.ObtenerDatosEscalaDeCalificacion(entidad);
        }

        [HttpPost]
        [Route("api/EliminarEscalaDeCalificacion")]
        public DataTable EliminarEscalaDeCalificacion(EntidadEscalasDeCalificacion entidad)
        {
            return DatosEscalasDeCalificacion.EliminarEscalaDeCalificacion(entidad);
        }

        [HttpPost]
        [Route("api/ActualizarEscalaDeCalificacion")]
        public DataTable ActualizarEscalaDeCalificacion(EntidadEscalasDeCalificacion entidad)
        {
            return DatosEscalasDeCalificacion.ActualizarEscalaDeCalificacion(entidad);
        }
    }
}